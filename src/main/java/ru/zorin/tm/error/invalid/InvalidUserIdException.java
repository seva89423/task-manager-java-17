package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidUserIdException extends AbstractException {

    public InvalidUserIdException () {
        super("User ID is invalid");
    }
}