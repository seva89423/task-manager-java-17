package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidLoginException extends AbstractException {

    public InvalidLoginException() {
        super("Invalid login");
    }
}