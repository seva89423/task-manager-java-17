package ru.zorin.tm.error.project;

import ru.zorin.tm.error.AbstractException;

public class ProjectEmptyException extends AbstractException {

    public ProjectEmptyException() {
        super("Project is empty");
    }
}