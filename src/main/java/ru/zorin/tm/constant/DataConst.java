package ru.zorin.tm.constant;

public class DataConst {

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_BIN = "./data.bin";
}