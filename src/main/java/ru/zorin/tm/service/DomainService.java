package ru.zorin.tm.service;

import ru.zorin.tm.api.service.*;
import ru.zorin.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public DomainService(IUserService userService, ITaskService taskService, IProjectService projectService) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void load(Domain domain) {
        if (domain == null) return;
    }

    @Override
    public void export(Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getProjectList());
        domain.setTasks(taskService.getTaskList());
        domain.setUsers(userService.getUsersList());
    }
}