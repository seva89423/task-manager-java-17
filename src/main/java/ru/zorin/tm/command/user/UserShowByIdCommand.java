package ru.zorin.tm.command.user;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-user-by-id";
    }

    @Override
    public String description() {
        return "Show information about user by id";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("[COMPLETE]");
    }
}
