package ru.zorin.tm.command.project;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;

import java.util.List;

public class ProjectShowListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "show-project-list";
    }

    @Override
    public String description() {
        return "Show list of projects";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        int index = 1;
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (Project project : projects) System.out.println(index +". "+ project);
        System.out.println("[COMPLETE]");
    }
}