package ru.zorin.tm.command.project;

import ru.zorin.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "clear-projects";
    }

    @Override
    public String description() {
        return "Clear all projects";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECT]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[COMPLETE]");
    }
}