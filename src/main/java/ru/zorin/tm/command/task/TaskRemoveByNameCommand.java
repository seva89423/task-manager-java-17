package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "remove-task-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) throw new TaskEmptyException();
        System.out.println("[COMPLETE]");
    }
}
