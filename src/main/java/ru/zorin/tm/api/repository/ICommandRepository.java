package ru.zorin.tm.api.repository;

import ru.zorin.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getTermCommands();
}