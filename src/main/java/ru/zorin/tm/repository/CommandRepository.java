package ru.zorin.tm.repository;

import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.command.auth.CommandLogin;
import ru.zorin.tm.command.auth.CommandLogout;
import ru.zorin.tm.command.auth.CommandRegistry;
import ru.zorin.tm.command.data.base64.DataBase64ClearCommand;
import ru.zorin.tm.command.data.base64.DataBase64LoadCommand;
import ru.zorin.tm.command.data.base64.DataBase64SaveCommand;
import ru.zorin.tm.command.data.bin.DataBinSaveCommand;
import ru.zorin.tm.command.data.bin.DataBinClearCommand;
import ru.zorin.tm.command.data.bin.DataBinLoadCommand;
import ru.zorin.tm.command.project.*;
import ru.zorin.tm.command.system.AboutCommand;
import ru.zorin.tm.command.system.ExitCommand;
import ru.zorin.tm.command.system.HelpCommand;
import ru.zorin.tm.command.system.SystemInfoCommand;
import ru.zorin.tm.command.task.*;
import ru.zorin.tm.command.user.*;
import ru.zorin.tm.command.user.lock.UserLockCommand;
import ru.zorin.tm.command.user.lock.UserUnlockCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class,
            SystemInfoCommand.class,
            AboutCommand.class,
            CommandLogin.class,
            CommandLogout.class,
            CommandRegistry.class,
            UserShowByIdCommand.class,
            UserShowByLoginCommand.class,
            UserUpdateByIdCommand.class,
            UserUpdateByLoginCommand.class,
            UserRemoveByIdCommand.class,
            UserRemoveByLoginCommand.class,
            TaskCreateCommand.class,
            TaskShowListCommand.class,
            TaskShowByIdCommand.class,
            TaskShowByNameCommand.class,
            TaskShowByIndexCommand.class,
            TaskUpdateByIdCommand.class,
            TaskRemoveByIndexCommand.class,
            TaskRemoveByIdCommand.class,
            TaskRemoveByNameCommand.class,
            TaskClearCommand.class,
            ProjectCreateCommand.class,
            ProjectShowListCommand.class,
            ProjectShowByIdCommand.class,
            ProjectShowByNameCommand.class,
            ProjectShowByIndexCommand.class,
            ProjectUpdateByIdCommand.class,
            ProjectRemoveByIndexCommand.class,
            ProjectRemoveByIdCommand.class,
            ProjectRemoveByNameCommand.class,
            UserLockCommand.class,
            UserUnlockCommand.class,
            DataBase64SaveCommand.class,
            DataBase64LoadCommand.class,
            DataBase64ClearCommand.class,
            DataBinSaveCommand.class,
            DataBinLoadCommand.class,
            DataBinClearCommand.class,
            ExitCommand.class,
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();
    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<AbstractCommand> getTermCommands() {
        return commandList;
    }
}